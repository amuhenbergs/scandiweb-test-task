<?php

namespace App\Core;

interface ProductTypeInterface
{
    public function showType(): string;
}
