<?php

namespace App\Core;

class Validator
{
    protected $data;

    protected $errors = [];

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function validateForm()
    {
        $this->validateSku();
        $this->validateName();
        $this->validatePrice();
        $this->validateType();
        $this->validateSize($this->data['type']);
        $this->validateWeight($this->data['type']);
        $this->validateHeight($this->data['type']);
        $this->validateWidth($this->data['type']);
        $this->validateLength($this->data['type']);

        return $this->errors;
    }
    
    private function validateSku()
    {
        $sku = trim($this->data['sku']);
        $exists = App::get('database')->check('products', $_POST['sku']);

        if (empty($sku)) {
            $this->addError('sku', 'Please, submit required data');
        } else if ($exists) {
            $this->addError('sku', 'SKU already exists. Please, enter different SKU');
        }
    }

    private function validateName()
    {
        $name = trim($this->data['name']);

        if (empty($name)) {
            $this->addError('name', 'Please, submit required data');
        }
    }

    private function validatePrice()
    {
        $price = $this->data['price'];

        if (empty($price)) {
            $this->addError('price', 'Please, submit required data');
        }
    }

    private function validateType()
    {
        $type = $this->data['type'];

        if (empty($type)) {
            $this->addError('type', 'Please, select required data');
        }
    }

    private function validateSize($type)
    {
        if ($type == 'dvd') {
            $attribute = $this->data['size'];

            if (empty($attribute)) {
                $this->addError('size', 'Please, submit required data');
            } else if ($attribute < 0) {
                $this->addError('size', 'Entered value cannot be negative');
            }
        }
    }

    private function validateWeight($type)
    {
        if ($type == 'book') {
            $attribute = $this->data['weight'];

            if (empty($attribute)) {
                $this->addError('weight', 'Please, submit required data');
            } else if ($attribute < 0) {
                $this->addError('size', 'Entered value cannot be negative');
            }
        }
    }

    private function validateHeight($type)
    {
        if ($type == 'furniture') {
            $attribute = $this->data['height'];

            if (empty($attribute)) {
                $this->addError('height', 'Please, submit required data');
            } else if ($attribute < 0) {
                $this->addError('size', 'Entered value cannot be negative');
            }
        }
    }

    private function validateWidth($type)
    {
        if ($type == 'furniture') {
            $attribute = $this->data['width'];

            if (empty($attribute)) {
                $this->addError('width', 'Please, submit required data');
            } else if ($attribute < 0) {
                $this->addError('size', 'Entered value cannot be negative');
            }
        }
    }

    private function validateLength($type)
    {
        if ($type == 'furniture') {
            $attribute = $this->data['length'];

            if (empty($attribute)) {
                $this->addError('length', 'Please, submit required data');
            } else if ($attribute < 0) {
                $this->addError('size', 'Entered value cannot be negative');
            }
        }
    }

    private function addError($key, $value)
    {
        $this->errors[$key] = $value;
    }
}
