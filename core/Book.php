<?php

namespace App\Core;

use App\Core\ProductTypeBuilder;
use App\Core\ProductTypeInterface;

class Book extends ProductTypeBuilder implements ProductTypeInterface
{
    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    public function showType(): string
    {
        return "Weight: {$this->product->weight} KG"; 
    }
}