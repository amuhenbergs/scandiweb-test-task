<?php

namespace App\Core;

use App\Core\ProductTypeBuilder;
use App\Core\ProductTypeInterface;

class Dvd extends ProductTypeBuilder implements ProductTypeInterface
{
    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    public function showType(): string
    {
        return "Size: {$this->product->size} MB";
    }
}