<?php

namespace App\Core;

class ProductTypeBuilder
{   
    public function getChildClass($product)
    {
        $klassName = ucfirst($product->type);
        $klass = "App\\Core\\$klassName";

        return new $klass($product);
    }
}
