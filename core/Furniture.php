<?php

namespace App\Core;

use App\Core\ProductTypeBuilder;
use App\Core\ProductTypeInterface;

class Furniture extends ProductTypeBuilder implements ProductTypeInterface
{
    private $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    public function showType(): string
    {
        return "Dimension: {$this->product->height}x{$this->product->width}x{$this->product->length}";
    }
}