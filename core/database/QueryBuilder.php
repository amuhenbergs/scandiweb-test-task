<?php

namespace App\Core\Database;

use PDO;
use Exception;

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function selectAll($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    public function insert($table, $parameters)
    {
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        try {            
            $statement = $this->pdo->prepare($sql);
            
            $statement->execute($parameters);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function delete($table, $parameters)
    {
        $sql = sprintf(
            'delete from %s where id in (%s)',
            $table,
            implode(', ', array_values($parameters))
        );

        try {
            $statement = $this->pdo->prepare($sql);

            $statement->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function check($table, $parameter)
    {
        $statement = $this->pdo->prepare("select * from {$table} where sku = '{$parameter}'");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}
