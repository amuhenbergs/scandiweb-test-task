<?php

function view($name, $data = [])
{
    extract($data);

    return require "../app/views/{$name}.view.php";
}

function redirect($path)
{
    header("Location: /{$path}");
}

function getPostData($data = [])
{
    foreach ($data as $key => $value) {
        $data[$key] = $value;
    };

    return $data;
}