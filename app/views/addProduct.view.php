<?php require('partials/head.php'); ?>

<nav>
    <div class="container">
        <div class="title">
            <h1>Product Add</h1>
        </div>
        <div class="button-container">
            <input class="button" type="submit" value="SAVE" form="products-add-form">
            <a href="/products" class="button">CANCEL</a>
        </div>
    </div>
</nav>

<hr class="shortened">

<section class="container">
    <form id="products-add-form" action="/products/add" method="POST">
        <div class="form-group">
            <label for="product-sku">SKU</label>
            <input type="text" id="product-sku" name="sku">
            <span class="error"><?= $errors['sku'] ?? '' ?></span>
        </div>
        <div class="form-group">
            <label for="product-name">Name</label>
            <input type="text" id="product-name" name="name">
            <span class="error"><?= $errors['name'] ?? '' ?></span>
        </div>
        <div class="form-group">
            <label for="product-price">Price ($)</label>
            <input type="number" step="0.01" min="0" id="product-price" name="price">
            <span class="error"><?= $errors['price'] ?? '' ?></span>
        </div>
        <div class="form-group">
            <label for="product-type">Type Switcher</label>
            <select id="product-type" name="type" onchange="formBuilder.buildFields()">
                <option value="">Type Switcher</option>
                <option value="dvd">DVD</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>
            <span class="error"><?= $errors['type'] ?? '' ?></span>
        </div>
        <div id="dynamic-fields"></div>
    </form>
</section>

<script>
    document.addEventListener('DOMContentLoaded', () => {
        const formData = <?= isset($postData) ? json_encode($postData) : json_encode(null); ?>;
        let passedArray = <?php echo isset($postData) ? json_encode($errors) : json_encode(null); ?>;
        formBuilder.setFormData(formData);
        formBuilder.getInputErrors(passedArray);
    });
</script>

<?php require('partials/footer.php'); ?>