<?php require('partials/head.php'); ?>

<nav>
    <div class="container">
        <div class="title">
            <h1>Product List</h1>
        </div>
        <div class="button-container">
            <a href="/products/add" class="button">ADD</a>
            <input class="button" type="submit" name="delete" value="MASS DELETE" form="products-list-form">
        </div>
    </div>
</nav>

<hr class="shortened">

<form id="products-list-form" method="POST" action="/products">
    <section class="products">
        <?php foreach($products as $product): ?>
            <div class="product-card">
                <div class="product-info">
                    <input type="checkbox" name="product-checkbox[]" value="<?= $product->id; ?>">
                    <div><?= $product->sku; ?></div>
                    <div><?= $product->name; ?></div>
                    <div><?= $product->price . ' $'; ?></div>
                    <div><?= $productTypeBuilder->getChildClass($product)->showType(); ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </section>
</form>

<?php require('partials/footer.php'); ?>