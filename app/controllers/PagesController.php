<?php

namespace App\Controllers;

class PagesController
{
    public function toHome()
    {
        return redirect('products');
    }

    public function home()
    {
        return view('products');
    }
    
    public function addProduct()
    {
        return view('addProduct');
    }
}