<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Validator;
use App\Core\ProductTypeBuilder;

class ProductsController
{
    public function index()
    {
        $products = App::get('database')->selectAll('products');
        $productTypeBuilder = new ProductTypeBuilder();

        return view('products', compact('products', 'productTypeBuilder'));
    }

    public function store()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $validation = new Validator($_POST);
            $errors = $validation->validateForm();
            $postData = getPostData($_POST);

            if (empty($errors)) {
                App::get('database')->insert('products', $postData);

                return redirect('products');
            } else {
                return view('addProduct', compact('errors', 'postData'));
            }
        }
    }

    public function delete()
    {
        if (isset($_POST['delete'])) {
            if (isset($_POST['product-checkbox'])) {
                App::get('database')->delete('products', $_POST['product-checkbox']);
            }
        }

        return redirect('products');
    }
}