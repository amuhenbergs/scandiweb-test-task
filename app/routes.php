<?php

$router->get('', 'PagesController@toHome');
$router->get('products', 'PagesController@home');
$router->get('products/add', 'PagesController@addProduct');

$router->get('products', 'ProductsController@index');
$router->post('products', 'ProductsController@delete');
$router->post('products/add', 'ProductsController@store');