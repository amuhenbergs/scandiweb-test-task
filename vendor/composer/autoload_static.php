<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita06ed316efbe9807d509f58c76fd1315
{
    public static $files = array (
        '5ec26a44593cffc3089bdca7ce7a56c3' => __DIR__ . '/../..' . '/core/helpers.php',
    );

    public static $classMap = array (
        'App\\Controllers\\PagesController' => __DIR__ . '/../..' . '/app/controllers/PagesController.php',
        'App\\Controllers\\ProductsController' => __DIR__ . '/../..' . '/app/controllers/ProductsController.php',
        'App\\Core\\App' => __DIR__ . '/../..' . '/core/App.php',
        'App\\Core\\Book' => __DIR__ . '/../..' . '/core/Book.php',
        'App\\Core\\Database\\Connection' => __DIR__ . '/../..' . '/core/database/Connection.php',
        'App\\Core\\Database\\QueryBuilder' => __DIR__ . '/../..' . '/core/database/QueryBuilder.php',
        'App\\Core\\Dvd' => __DIR__ . '/../..' . '/core/Dvd.php',
        'App\\Core\\Furniture' => __DIR__ . '/../..' . '/core/Furniture.php',
        'App\\Core\\ProductTypeBuilder' => __DIR__ . '/../..' . '/core/ProductTypeBuilder.php',
        'App\\Core\\ProductTypeInterface' => __DIR__ . '/../..' . '/core/ProductTypeInterface.php',
        'App\\Core\\Request' => __DIR__ . '/../..' . '/core/Request.php',
        'App\\Core\\Router' => __DIR__ . '/../..' . '/core/Router.php',
        'App\\Core\\Validator' => __DIR__ . '/../..' . '/core/Validator.php',
        'ComposerAutoloaderInita06ed316efbe9807d509f58c76fd1315' => __DIR__ . '/..' . '/composer/autoload_real.php',
        'Composer\\Autoload\\ClassLoader' => __DIR__ . '/..' . '/composer/ClassLoader.php',
        'Composer\\Autoload\\ComposerStaticInita06ed316efbe9807d509f58c76fd1315' => __DIR__ . '/..' . '/composer/autoload_static.php',
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInita06ed316efbe9807d509f58c76fd1315::$classMap;

        }, null, ClassLoader::class);
    }
}
