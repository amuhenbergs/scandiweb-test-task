<?php

return [
    'database' => [
        'name' => 'db_name',
        'username' => 'db_username',
        'password' => 'db_password',
        'connection' => 'mysql:host=db_connection',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ]
];