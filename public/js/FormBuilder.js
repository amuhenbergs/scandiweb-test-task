class FormBuilder {
  getFormFieldsByType(type) {
    switch(type) {
      case 'dvd':
        return this.getDvdFormFields();
      case 'book':
        return this.getBookFormFields();
      case 'furniture':
        return this.getFurnitureFormFields();
    }
  }

  getInputRows(attributeName, labelName, attributeDescription) {
    let output = '';
  
    for (let i = 0; i < labelName.length; i++) {
      output += `<div class="form-group">
        <label for="product-${attributeName[i]}">${labelName[i]}</label>
        <input type="number" step="0.01" name="${attributeName[i]}" id="product-${attributeName[i]}">
        <span class="error-message"></span>
      </div>`
    }

    output += `<div class="product-description">Please, provide ${attributeDescription}</div>`

    return output;
  }

  getDvdFormFields() {
    const dvdAttributes = ['size'];
    
    const dvdAttributeNames = ['Size (MB)'];

    return this.getInputRows(dvdAttributes, dvdAttributeNames, 'size');
}

  getBookFormFields() {
    const bookAttributes = ['weight'];

    const bookAttributeNames = ['Weight (KG)'];
        
    return this.getInputRows(bookAttributes, bookAttributeNames, 'weight');
}

  getFurnitureFormFields() {
    const furnitureAttributes = [
      'height',
      'width',
      'length'
    ];

    const furnitureAttributeNames = [
      'Height (CM)',
      'Width (CM)',
      'Length (CM)'
    ];

    return this.getInputRows(furnitureAttributes, furnitureAttributeNames, 'dimensions');
  }

  buildFields() {
      const typeList = document.getElementById('product-type');
      const dynamicFields = this.getFormFieldsByType(typeList.value);
      const appendTo = document.getElementById('dynamic-fields');

      if (dynamicFields) {
        appendTo.innerHTML = dynamicFields;
      }
  }

  setFormData(data) {
    if (! data) {
      return;
    }

    const { sku, name, price, type, size, weight, height, width, length } = data;

    document.getElementById('product-sku').value = sku;
    document.getElementById('product-name').value = name;
    document.getElementById('product-price').value = price;
    document.getElementById('product-type').value = type;

    this.buildFields();

    switch (type) {
      case 'dvd':
        document.getElementById('product-size').value = size;
        break;
      case 'book':
        document.getElementById('product-weight').value = weight;
        break;
      case 'furniture':
        document.getElementById('product-height').value = height;
        document.getElementById('product-width').value = width;
        document.getElementById('product-length').value = length;
        break;
    }
  }

  getInputErrors(errorData) {
    if (! errorData) {
      return {};
    }

    const { size, weight, height, width, length } = errorData;

    if ('size' in errorData) document.getElementsByClassName('error-message')[0].innerHTML = size;

    if ('weight' in errorData) document.getElementsByClassName('error-message')[0].innerHTML = weight;

    if ('height' in errorData) document.getElementsByClassName('error-message')[0].innerHTML = height;

    if ('width' in errorData) document.getElementsByClassName('error-message')[1].innerHTML = width;

    if ('length' in errorData) document.getElementsByClassName('error-message')[2].innerHTML = length;
  }
}

const formBuilder = new FormBuilder();